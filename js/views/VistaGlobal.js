Marvel.Views = Marvel.Views || {};

(function () {
    'use strict';

    Marvel.Views.VistaGlobal = Mn.LayoutView.extend({
        template: false,
        el: 'body',
        regions: {
            cabecera: '#cabecera',
            formBusqueda: "#formBusqueda",
            listado: '#listado'
        }
    });
})();
